﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="plotting.aspx.cs" Inherits="bonusAssignment.plotting" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Enter value of x:</label>
            <asp:TextBox runat="server" ID="xValue"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="xValue" ErrorMessage="X cannot be zero"></asp:RequiredFieldValidator>
            <br />
            <label>Enter Value of y:</label>
            <asp:TextBox runat="server" ID="yValue"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="yValue" ErrorMessage="Y cannot be Zero"></asp:RequiredFieldValidator>
            <br />
            <asp:Button runat="server" ID="plotit" Text="plot it" OnClick="plotit_Click" />
        </div>
        <div runat="server" id="plottwist">

        </div>
    </form>
</body>
</html>
