﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Primenumber.aspx.cs" Inherits="bonusAssignment.Palindrome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server">Enter Integer</asp:Label>
            <asp:TextBox runat="server" ID="primeIdentifier"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="requiredfieldvalidate" ControlToValidate="primeIdentifier" ErrorMessage="Entera valid number"></asp:RequiredFieldValidator>
                        
        </div>
        <div>
            <asp:Button runat="server" ID="sumbitbtn" Text="Check for prime" OnClick="Primechecker" />
        </div>
        <div runat="server" id="primeoutput">
            
        </div>
    </form>
</body>
</html>
