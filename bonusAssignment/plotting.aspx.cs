﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace bonusAssignment
{
    public partial class plotting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void plotit_Click(object sender, EventArgs e)
        {
            int x, y;
            x = Convert.ToInt32(xValue.Text);
            y = Convert.ToInt32(yValue.Text);
            if(x>0 && y>0)
            {
                plottwist.InnerHtml = "(" + x +","+ y +") Falls in quadrant 1";
            }
            else if (x<0 && y>0)
            {
                plottwist.InnerHtml = "(" + x + "," + y + ") Falls in quadrant 2";
            }
            else if (x>0 && y<0)
            {
                plottwist.InnerHtml = "(" + x + "," + y + ") Falls in quadrant 3"; 
            }
            else if(x<0 && y<0)
            {
                plottwist.InnerHtml = "(" + x + "," + y + ") Falls in quadrant 4";
            }
            else
            {
                plottwist.InnerHtml = "Enter valid input";
            }
        }
    }
}