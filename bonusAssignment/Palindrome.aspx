﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Palindrome.aspx.cs" Inherits="bonusAssignment.Palindrome1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label runat="server">Enter string</label>
            <asp:TextBox runat="server" ID="findpalindrome"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="validatepalindorm" ControlToValidate="findpalindrome" ErrorMessage="Enter the string"></asp:RequiredFieldValidator>
        </div>
        <div>
            <asp:Button runat="server" ID="primebutton" Text="Check Palindrome" OnClick="palindromecheck" />
        </div>
        <div runat="server" id="palidromeOut">

        </div>
    </form>
</body>
</html>
