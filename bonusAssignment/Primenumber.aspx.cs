﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace bonusAssignment
{
    public partial class Palindrome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Primechecker(object sender, EventArgs e)
        {
            int count = 0;
            int x;
            x = Convert.ToInt32(primeIdentifier.Text);
            int result = Check_Prime(x);
            if(result == 0)
            {
                primeoutput.InnerHtml = "Its not a prime number";
            }
            else
            {
                primeoutput.InnerHtml = "Its a prime number";
            }
        }

        private static int Check_Prime(int number)
        {
            int i;
            for (i = 2; i <= number - 1; i++)
            {
                if (number % i == 0)
                {
                    return 0;
                }
            }
            if (i == number)
            {
                return 1;
            }
            return 0;
        }
    }
}